class Code
  #CODE objects are represented as an array of lower case
  #letters representing the colors

  attr_reader :pegs

  PEGS = {
    "r" => "red",
    "g" => "green",
    "b" => "blue",
    "y" => "yellow",
    "o" => "orange",
    "p" => "purple"
  }

  def initialize(code)
    @pegs = code
  end

  #parse took a user input string like "rgby" and builds
  # a Code object: an array with the color names in order
  #ie ["r","g","g","b"]
  def self.parse(input)
    input_code = input.split("").map do |color|
      color = color.downcase
      if PEGS.include?(color) == false
        raise "error"
      end
      color
    end
    Code.new(input_code)
  end


  def self.random
    #get array of peg color values
    color_values = PEGS.keys
    random_code = []
    4.times do
      random_code << color_values[rand(color_values.size)]
    end
    self.new(random_code)
  end

  #given an index, will return the color of the code object
  def [](idx)
    @pegs[idx]
  end

  #will compare current code with input "other_code", which will already be parsed and a is a code object
  #other code is a code object, need to specify .pegs
  def exact_matches(other_code)
    #compare all downcase
    matches = 0
    @pegs.each_with_index do |color,index|
      matches+=1 if color == other_code.pegs[index]
    end
    return matches
  end

  def near_matches(other_code)
    near_matches = 0
    other_code_holder = other_code.pegs

    #get rid of exact matches from other code
    exact_indices = []
    @pegs.each_with_index do |color,index|
      if color == other_code_holder[index]
        other_code_holder[index] = nil
        exact_indices << index
      end
    end

    @pegs.each_with_index do |color,index|
      #next because we can't alter @pegs
      next if exact_indices.include?(index)
      other_code_holder.each_with_index do |color2,index2|
        if color == color2 && index != index2
          near_matches += 1
          other_code_holder[index2] = nil
          break
        end
      end
    end
    near_matches
  end

  def == (code)
    return false if code.is_a?(Code) == false
    code.pegs == self.pegs
  end

end

class Game
  attr_reader :secret_code
  #how many turns have passed
  #the correct code
  #prompt user for input

  #optional arguments use a default in the argument space
  def initialize(secret_code = Code.random)
    @secret_code = secret_code

  end

  #returns user guess as a code object
  def get_guess
    puts "Input Guess"
    begin
    #gets.chomp() = read ARGV first
    #STDIN.gets.chomp() = read user's input
    #ARGV.clear
      ARGV.clear
      guess = $stdin.gets.chomp
      Code.parse(guess)
    #rescue occurs if code in begin raises an error
    #need begin/rescue because #parse contains options for an error
    rescue
      puts "Enter a valid code:"
      retry
    end
  end

    def display_matches(code)
      print "#{@secret_code.exact_matches(code)} exact"
      print "#{@secret_code.near_matches(code)} near"
    end

    def play
      guess_object = self.get_guess
      number_of_guesses = 1
      while @secret_code.==(guess_object) == false
        self.display_matches(guess_object)
        guess_object = self.get_guess
        number_of_guesses += 1
        if number_of_guesses == 10
          puts "You Lose"
          break
        end
      end

      if @secret_code.==(guess_object) == true
        puts "You won in #{number_of_guesses} moves"
      end
    end
end

if __FILE__ == $PROGRAM_NAME
  new_game = Game.new
  new_game.play
end
